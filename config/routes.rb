Senchatest::Application.routes.draw do
  
  root :to => 'home#index'

  namespace :api do
    resources :notes,  :except => [ :edit, :new, :show ]
  end
end