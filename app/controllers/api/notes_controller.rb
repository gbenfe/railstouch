class Api::NotesController < Api::BaseController
  def index
    @collection = Note.all
    render json: { success: true, total: @collection.count, rows: @collection}
  end

  def create
    @record = Note.new params[:note]

    if @record.save
      render json: @record, status: :created
    else
      render json: @record.errors, status: :unprocessable_entity
    end
  end

  def update
    @record = Note.find params[:id]

    if @record.update_attributes params[:note]
      head :no_content
    else
      render json: @record.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @record = Note.find params[:id] 
    @record.destroy
    head :no_content
  end
end