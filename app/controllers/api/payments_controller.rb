class Api::PaymentsController < Api::BaseController
  def index
    @collection = Payment.all
    render json: { success: true, total: @collection.count, rows: @collection}
  end

  def create
    @record = Payment.new params[:payment]

    if @record.save
      render json: @record, status: :created
    else
      render json: @record.errors, status: :unprocessable_entity
    end
  end

  def update
    @record = Payment.find params[:id]

    if @record.update_attributes params[:payment]
      head :no_content
    else
      render json: @record.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @record = Payment.find params[:id] 
    @record.destroy
    head :no_content
  end
end