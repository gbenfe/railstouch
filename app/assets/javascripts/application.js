// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require touch/sencha-touch-all-debug.js
//= require app/model/Note
//= require app/store/NotesStore
//= require app/view/EditNote
//= require app/view/ListNotes
//= require app/view/default/MainView
//= require app/view/phone/MainView
//= require app/profile/Default
//= require app/profile/Phone
//= require app/controller/BaseController
//= require app/controller/default/DefaultController
//= require app/controller/phone/PhoneController
//= require app