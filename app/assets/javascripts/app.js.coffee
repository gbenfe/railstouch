# head = document.head;
# addMeta = (name, content) ->
#   meta = document.createElement('meta');
#   meta.setAttribute('name', name);
#   meta.setAttribute('content', content);
#   head.appendChild(meta);
#   return

# if navigator.userAgent.match(/IEMobile\/10\.0/)
#   msViewportStyle = document.createElement("style");
#   msViewportStyle.appendChild(
#     document.createTextNode(
#       "@media screen and (orientation: portrait) {" +
#           "@-ms-viewport {width: 320px !important;}" +
#       "}" +
#       "@media screen and (orientation: landscape) {" +
#           "@-ms-viewport {width: 560px !important;}" +
#       "}"
#     )
#   )
#   document.getElementsByTagName("head")[0].appendChild(msViewportStyle)

# addMeta('viewport', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no');
# addMeta('apple-mobile-web-app-capable', 'yes');
# addMeta('apple-touch-fullscreen', 'yes');


Ext.Loader.setPath
    'Railstouch': '/assets/app'

Ext.application
  name: 'Railstouch'
  profiles: [ 'Phone', 'Default' ]
  models: [ 'Note']
  views: [ 'ListNotes', 'EditNote' ]
  controllers: [ 'BaseController' ]
  stores: [ 'NotesStore' ]
  icon:
    '57': '/assets/touch/resources/icons/Icon.png'
    '72': '/assets/touch/resources/icons/Icon~ipad.png'
    '114': '/assets/touch/resources/icons/Icon@2x.png'
    '144': '/assets/touch/resources/icons/Icon~ipad@2x.png'
  isIconPrecomposed: true
  startupImage:
    '320x460': '/assets/touch/resources/startup/320x460.jpg'
    '640x920': '/assets/touch/resources/startup/640x920.png'
    '768x1004': '/assets/touch/resources/startup/768x1004.png'
    '748x1024': '/assets/touch/resources/startup/748x1024.png'
    '1536x2008': '/assets/touch/resources/startup/1536x2008.png'
    '1496x2048': '/assets/touch/resources/startup/1496x2048.png'

  # launch: ->
  #   Ext.create 'Railstouch.view.MainView',
  #     fullscreen: true
  #   return
return