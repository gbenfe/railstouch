Ext.define 'Railstouch.store.NotesStore',
  extend: 'Ext.data.Store'
  requires: [
    'Ext.data.proxy.Rest'
  ]
  config:
    storeId: 'notesstore'
    autoLoad: true
    model: 'Railstouch.model.Note'
    sorters: [
      {
        property:'updated_at'
        direction: 'DESC'
      }
    ]

    proxy:
      type: 'rest'
      url: 'api/notes'

      reader:
        type: 'json'
        rootProperty: 'rows'

      writer:
        type: 'json'