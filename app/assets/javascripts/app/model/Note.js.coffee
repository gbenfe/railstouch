Ext.define "Railstouch.model.Note",
  extend: "Ext.data.Model"
  config:
    fields: [
      { name: 'id', type: 'int' }
      { name: 'title', type: 'string' }
      { name: 'text', type: 'string' }
      { name: 'updated_at', type: 'string' }
    ]