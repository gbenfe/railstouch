Ext.define 'Railstouch.view.ListNotes',
  extend: 'Ext.dataview.List'
  xtype: 'listnotes'
  alias: 'widget.listnotes'
  requires: [
    'Ext.dataview.List'
  ]
  config:
    id: 'listnotes'
    store: 'NotesStore'
    itemTpl: '<div>{title} {text}</div>'
    itemTpl: '<div><div class="note-title">{title}</div><p>{text}</p><div class="note-date">{updated_at:date("Y-m-d H:i:s")}</div></div>'
    store: 'notesstore'
    disableSelection: true