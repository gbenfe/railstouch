Ext.define 'Railstouch.view.default.MainView',
  extend: 'Ext.navigation.View'

  requires: [
    'Railstouch.view.ListNotes'
    'Railstouch.view.EditNote'
  ]

  config:
    id: 'containernotes'
    navigationBar:
      items: [
        {
          xtype: 'button'
          iconCls: 'add'
          ui: 'plain'
          action: 'newnote'
        }
      ]

    items: [
      {
        xtype: 'container'
        title: 'Railstouch'
        layout: 'hbox'
        items: [
          {
            xtype: 'listnotes'
            flex: 1
          }
          {
            xtype: 'editnote'
            flex: 2
          }
        ]
      }
    ]