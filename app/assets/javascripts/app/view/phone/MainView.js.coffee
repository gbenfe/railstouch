Ext.define 'Railstouch.view.phone.MainView',
  extend: 'Ext.navigation.View'
  requires: [
    'Railstouch.view.ListNotes'
  ]
  config:
    id: 'containernotes'
    navigationBar:
      items: [
        {
          xtype: 'button'
          iconCls: 'add'
          ui: 'plain'
          action: 'newnote'
        }
      ]
    items: [
      {
        xtype: 'listnotes'
        title: 'Railstouch'
      }
    ]