Ext.define 'Railstouch.view.EditNote',
  extend: 'Ext.form.Panel'
  xtype: 'editnote'
  alias: 'widget.editnote'
  requires: [
    'Ext.form.FieldSet'
    'Ext.field.Text'
    'Ext.field.TextArea'
    'Ext.Toolbar'
  ]
  config:
    id: 'editnote'
    title: 'Edit note'
    items: [
      {
        xtype: 'fieldset'
        title: 'Edit note'
        instructions: 'Complete note'
        items: [
          {
            xtype: 'textfield'
            name: 'title'
            label: 'Title'
            required: true
          }
          {
            xtype: 'textareafield'
            name: 'text'
            label: 'Text'
          }
        ]
      }
      {
        xtype: 'toolbar'
        docked: 'bottom'
        items: [
          {
            xtype: 'button'
            iconCls: 'trash'
            ui: 'plain'
            action: 'deletenote'
          }
          {
            xtype: 'spacer'
          }
          {
            xtype: 'button'
            iconCls: 'star'
            ui: 'plain'
            action: 'savenote'
          }
        ]
      }
    ]