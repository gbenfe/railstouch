Ext.define 'Railstouch.profile.Phone',
  extend: 'Ext.app.Profile'

  requires: [ 'Railstouch.view.phone.MainView' ]

  config:
    name: 'Phone'
    views: [ 'MainView' ]
    controllers: [ 'PhoneController']
    
  isActive: ->
    Ext.os.deviceType == 'Phone'

  launch: ->
    console.log 'Phone'
    Ext.create 'Railstouch.view.phone.MainView',
      fullscreen: true