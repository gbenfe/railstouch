Ext.define 'Railstouch.profile.Default',
  extend: 'Ext.app.Profile'

  requires: [ 'Railstouch.view.default.MainView' ]

  config:
    name: 'Default'
    views: [ 'MainView' ]
    controllers: [ 'DefaultController']
    
  isActive: ->
    Ext.os.deviceType =='Tablet' || Ext.os.deviceType =='Desktop'

  launch: ->
    Ext.create 'Railstouch.view.default.MainView',
      fullscreen: true