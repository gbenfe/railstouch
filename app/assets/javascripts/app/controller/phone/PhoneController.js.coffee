Ext.define 'Railstouch.controller.phone.PhoneController',
  extend: 'Railstouch.controller.BaseController'

  requires: [
    'Ext.MessageBox'
  ]

  config:
    control:
      newnotebtn:
        tap: 'newNoteBtnTapHandler'
      savenotebtn:
        tap: 'saveNoteBtnTapHandler'
      deletenotebtn:
        tap: 'deleteNoteBtnTapHandler'
      listnotes:
        itemtap: 'listnotesItemtapHandler'
      containernotes:
        pop: 'containernotesPopHandler'

  newNoteBtnTapHandler: (element, e, eOpts) ->
    now = new Date()
    record = Ext.create 'Railstouch.model.Note',
      id: null
      updated_at: now
      title: ''
      text: ''
    @getEditnote().setRecord record
    element.hide()
    @getContainernotes().push(@getEditnote())
    return

  saveNoteBtnTapHandler: ( element, e, eOpts ) ->
    record = @getEditnote().getRecord()
    @getEditnote().updateRecord( record )
    # noteError = record.validate()
    # if(!noteError.isValid()){
    #   Ext.Msg.alert('Attention', noteError.getByField('title')[0].getMessage());
    #   return;
    # }
    store = @getListnotes().getStore()
    store.add( record )
    store.sync()
    @getContainernotes().pop()
    return

  deleteNoteBtnTapHandler: ( element, e, eOpts ) ->
    Ext.Msg.confirm 'Delete note', 'Do you remove current note?', ( buttonId, value, opt ) =>
      if buttonId is 'yes'
        note = @getEditnote().getRecord();
        store = @getListnotes().getStore();
        store.remove( note );
        store.sync();
        @getContainernotes().pop();
    return

  listnotesItemtapHandler: ( element, index, target, record, e, eOpts ) ->
    @getEditnote().setRecord( record );
    @getNewnotebtn().hide();
    @getContainernotes().push( @getEditnote() );
    return

  containernotesPopHandler: ( element, view, eOpts ) ->
    @getNewnotebtn().show()

return