Ext.define 'Railstouch.controller.default.DefaultController',
  extend: 'Railstouch.controller.BaseController'
  
  requires: [
    'Ext.MessageBox'
    'Railstouch.view.EditNote'
  ]

  config:
    control:
      newnotebtn:
        tap: 'newNoteBtnTapHandler'
      savenotebtn:
        tap: 'saveNoteBtnTapHandler'
      deletenotebtn:
        tap: 'deleteNoteBtnTapHandler'
      listnotes:
        itemtap: 'listnotesItemtapHandler'

  newNoteBtnTapHandler: (element, e, eOpts) ->
    @getEditnote().enable()
    now = new Date()
    record = Ext.create 'Railstouch.model.Note',
      id: null
      updated_at: now
      title: ''
      text: ''
    @getEditnote().setRecord record
    return

  saveNoteBtnTapHandler: (element, e, eOpts) ->
    record = @getEditnote().getRecord()
    @getEditnote().updateRecord( record )
    # noteError = record.validate()
    # if(!noteError.isValid()){
    #   Ext.Msg.alert('Attention', noteError.getByField('title')[0].getMessage());
    #   return;
    # }
    store = @getListnotes().getStore()
    store.getProxy().setExtraParam("name","luigi")
    store.add( record )
    store.sync()
    @getEditnote().reset()
    @getEditnote().disable()
    return

  deleteNoteBtnTapHandler: (element, e, eOpts) ->
    Ext.Msg.confirm 'Delete note', 'Do you remove current note?', (buttonId, value, opt) =>
      if buttonId is 'yes'
        note = @getEditnote().getRecord()
        store = @getListnotes().getStore()
        store.remove( note )
        store.sync()
        @getEditnote().reset()
        @getEditnote().disable()
    return

  listnotesItemtapHandler: (element, index, target, record, e, eOpts) ->
    @getEditnote().enable()
    @getEditnote().setRecord( record );
    return

  launch: ->
    @getEditnote().disable()
    return

return