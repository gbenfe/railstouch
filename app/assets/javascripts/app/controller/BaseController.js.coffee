Ext.define 'Railstouch.controller.BaseController',
  extend: 'Ext.app.Controller'
  requires: [
    'Ext.MessageBox'
  ]
  config:
    refs:
      # components
      newnotebtn: 'button[action=newnote]'
      savenotebtn: 'button[action=savenote]'
      deletenotebtn: 'button[action=deletenote]'

  #     # containers
      containernotes: '#containernotes'
      listnotes:
        selector: '#listnotes' #si riferisce a id
        xtype: 'listnotes' #si riferisce a alias
        autoCreate: true
      editnote:
        selector: '#editnote' #si riferisce a id
        xtype: 'editnote' #si riferisce a alias
        autoCreate: true
  #     maineditnote:
  #       selector: '#maineditnote' #si riferisce a id
  #       xtype: 'maineditnote' #si riferisce a alias
  #       autoCreate: true

    # control:
      # newnotebtn:
      #   tap: 'newNoteBtnTapHandler'
      # savenotebtn:
      #   tap: 'saveNoteBtnTapHandler'
      # deletenotebtn:
      #   tap: 'deleteNoteBtnTapHandler'
  #     listnotes:
  #       itemtap: 'listnotesItemtapHandler'
  #     containernotes:
  #       pop: 'containernotesBackHandler'

  # newNoteBtnTapHandler: (element, e, eOpts) ->
  #   console.log element, e, eOpts
  #   Ext.Msg.alert "New note MainController"
  #   now = new Date()
  #   record = Ext.create 'Railstouch.model.Note',
  #     id: null
  #     updated_at: now
  #     title: ''
  #     text: ''
  #   @getEditnote().setRecord record
  #   element.hide()
  #   @getContainernotes().push(@getEditnote())
    # return

  # saveNoteBtnTapHandler: (element, e, eOpts) ->
  #   Ext.Msg.alert "Save note MainController"
  #   record = this.getEditnote().getRecord()
  #   @getEditnote().updateRecord( record )
  #   # noteError = record.validate()
  #   # if(!noteError.isValid()){
  #   #   Ext.Msg.alert('Attention', noteError.getByField('title')[0].getMessage());
  #   #   return;
  #   # }
  #   store = @getListnotes().getStore()
  #   store.add( record )
  #   store.sync()
  #   @getContainernotes().pop()
    # return

  # deleteNoteBtnTapHandler: (element, e, eOpts) ->
  #   Ext.Msg.alert "Delete note MainController"
  # #   Ext.Msg.confirm 'Delete note', 'Do you remove current note?', (buttonId, value, opt) =>
  # #     if buttonId is 'yes'
  # #       note = @getEditnote().getRecord();
  # #       store = @getListnotes().getStore();
  # #       store.remove( note );
  # #       store.sync();
  # #       @getContainernotes().pop();
  #   return

  # listnotesItemtapHandler: (element, index, target, record, e, eOpts) ->
  #   @getEditnote().setRecord( record );
  #   @getNewnotebtn().hide();
  #   @getContainernotes().push( @getEditnote() );
  #   return

  # containernotesBackHandler: (element, eOpts) ->
  #   @getNewnotebtn().show();
  #   return

return