## Rails Touch

Ho realizzato un semplice test per la crezione di una web application mobile con
il framework Sencha Touch e interfaccia REST, realizzata con Rails, per
la persistenza dei dati. 
Ho inoltre integrato il framework Sencha Touch nella assets pipeline di Rails
per avere la possibilità di scrivere l'applicazione in Coffeescript.

## Requisiti:
* Ruby 1.9.3 e Rubygem

## Installazione:
* Scaricare il repository
* Via terminale entrare nella directory del progetto
* Scaricare le dipendenze del progetto
`bundle install`
* Inizializzare il db
`rake db:create`
poi
`rake db:migrate`
* Lanciare il server rails
`rails s`
* Accedere alla home del progetto con un web browser all'indirizzo
[http://localhost:3000](http://localhost:3000)

## Online demo
[http://rails.exitweb.it:3000(http://rails.exitweb.it:3000)